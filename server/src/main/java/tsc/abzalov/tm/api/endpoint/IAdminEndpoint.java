package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.User;

import java.util.List;

public interface IAdminEndpoint {

    void createUserWithCustomRole(@Nullable Session session, @Nullable String login,
                                  @Nullable String password, @Nullable Role role,
                                  @Nullable String firstName, @Nullable String lastName,
                                  @Nullable String email);

    void deleteUserByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User lockUnlockUserById(@Nullable Session session, @Nullable String id);

    @Nullable
    User lockUnlockUserByLogin(@Nullable Session session, @Nullable String login);

    long sizeUsers(@Nullable Session session);

    boolean isEmptyUserList(@Nullable Session session);

    void createUserWithEntity(@Nullable Session session, @Nullable User user);

    void addAllUsers(@Nullable Session session, @Nullable List<User> users);

    @NotNull
    List<User> findAllUsers(@Nullable Session session);

    @Nullable
    User findUsersById(@Nullable Session session, @Nullable String id);

    void clearAllUsers(@Nullable Session session);

    void removeUserById(@Nullable Session session, @Nullable String id);

    void createUser(@Nullable Session session, @Nullable final String login,
                    @Nullable final String password, @Nullable final String firstName,
                    @Nullable final String lastName, @Nullable final String email);

    boolean isUserExist(@Nullable Session session, @Nullable String login, @Nullable String email);

    @Nullable
    User findUserById(@Nullable Session session, @Nullable String userId);

    @Nullable
    User findUserByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User editPasswordById(@Nullable Session session, @Nullable String userId, @Nullable String newPassword);

    @Nullable
    User editUserInfoById(@Nullable Session session, @Nullable String userId,
                          @Nullable String firstName, @Nullable String lastName);

}
