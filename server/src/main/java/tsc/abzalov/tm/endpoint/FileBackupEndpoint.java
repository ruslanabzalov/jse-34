package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.endpoint.IFileBackupEndpoint;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class FileBackupEndpoint extends AbstractEndpoint implements IFileBackupEndpoint {

    public FileBackupEndpoint() {
    }

    public FileBackupEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void autoLoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.autoLoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void autoSaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.autoSaveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void base64LoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.base64LoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void base64SaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.base64SaveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void binaryLoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.binaryLoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void binarySaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.binarySaveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void fasterXmlJsonLoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.fasterXmlJsonLoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void fasterXmlJsonSaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.fasterXmlJsonSaveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void fasterXmlLoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.fasterXmlLoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void fasterXmlSaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.fasterXmlSaveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void fasterXmlYamlLoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.fasterXmlYamlLoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void fasterXmlYamlSaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.fasterXmlYamlSaveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void jaxbJsonLoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.jaxbJsonLoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void jaxbJsonSaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.jaxbJsonSaveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void jaxbXmlLoadBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.jaxbXmlLoadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void jaxbXmlSaveBackup(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val fileBackupService = getEndpointLocator().getFileBackupService();
        fileBackupService.jaxbXmlSaveBackup();
    }

}
