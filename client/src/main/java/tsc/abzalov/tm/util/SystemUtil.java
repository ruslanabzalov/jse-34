package tsc.abzalov.tm.util;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

@UtilityClass
public class SystemUtil {

    @NotNull
    public static final Encoder BASE64_ENCODER = Base64.getEncoder();

    @NotNull
    public static final Decoder BASE64_DECODER = Base64.getDecoder();

    public static long getApplicationPid() {
        @Nullable val processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            }
            catch (@NotNull final Exception exception) {
                return 0;
            }
        }
        return 0;
    }

}
