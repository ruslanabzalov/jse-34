package tsc.abzalov.tm.command.user;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.CannotLockCurrentUserException;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputLogin;


public final class UserLockUnlockByLoginCommand extends AbstractCommand {

    public UserLockUnlockByLoginCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "lock-unlock-user-by-login";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Lock/Unlock user by login.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("LOCK/UNLOCK USER BY LOGIN");
        @NotNull val userLogin = inputLogin();
        @NotNull val session = getServiceLocator().getSession();
        @NotNull val userId = session.getUserId();
        @NotNull val userEndpoint = getServiceLocator().getUserEndpoint();
        @NotNull val currentUserLogin = userEndpoint.findUserById(session, userId).getLogin();

        if (userLogin.equals(currentUserLogin)) throw new CannotLockCurrentUserException();

        @NotNull val adminEndpoint = getServiceLocator().getAdminEndpoint();
        @Nullable val lockedUnlockedUser = adminEndpoint.lockUnlockUserByLogin(session, userLogin);
        Optional.ofNullable(lockedUnlockedUser).orElseThrow(UserIsNotExistException::new);

        System.out.println("User successfully locked/unlocked by admin.\n");
    }

}
