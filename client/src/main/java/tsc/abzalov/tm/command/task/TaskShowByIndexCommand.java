package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputIndex;


public final class TaskShowByIndexCommand extends AbstractCommand {

    public TaskShowByIndexCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-task-by-index";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by index.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("FIND TASK BY INDEX");
        @NotNull val taskEndpoint = getServiceLocator().getTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areTasksExist = taskEndpoint.tasksSize(session) != 0;
        if (areTasksExist) {
            val taskIndex = inputIndex();
            System.out.println();

            @Nullable val searchedTask = taskEndpoint.findTaskByIndex(session, taskIndex);
            val isTaskExist = Optional.ofNullable(searchedTask).isPresent();
            if (isTaskExist) {
                val taskOutputIndex = taskEndpoint.taskIndex(session, searchedTask) + 1;
                System.out.println(taskOutputIndex + ". " + searchedTask + "\n");
                return;
            }

            System.out.println("Searched task was not found.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
